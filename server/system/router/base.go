/*
 * @Date: 2022-04-12 09:17:10
 * @LastEditors: 春贰
 * @gitee: https://gitee.com/chun22222222
 * @github: https://github.com/chun222
 * @Desc:
 * @LastEditTime: 2023-03-12 22:35:50
 * @FilePath: \server\system\router\base.go
 */

package router

import (
	"chunDoc/system/controller/base"
	"chunDoc/system/middleware"
	"github.com/gin-gonic/gin"
)

func BaseRouter(r *gin.Engine) {

	//无需登录
	r.POST("/login", base.Login)
	r.POST("/doclist", base.List)
	r.POST("/read", base.ReadContent)
	r.POST("/search", base.Search)
	r.POST("/allconfig", base.AllConfig)

	// authG := r.Group("tools")
	// //只需要登录
	r.Use(middleware.JwtMid())
	{
		r.POST("/saveconfigs", base.SaveConfigs)
		r.POST("/savecontent", base.SaveContent)
		r.POST("/create_update_file", base.CreateOrUpdateFile)
		r.POST("/removefile", base.RemoveFile)
		r.POST("/uploadfile", base.Upload)
	}

}
